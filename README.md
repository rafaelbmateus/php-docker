# PHP Docker

Esse projeto é um exemplo de como dockerizar uma aplicação PHP,
utilizando o [GitLab CI](https://docs.gitlab.com/ee/ci/)
como *continuous integration* e *continuous delivery*.

Jobs executados na pipeline do CI:

1. build: Cria o build do projeto.
2. lint: Faz uma análise estática do código.
3. test: Roda os testes e validações.
4. release: Da *push* da tag para o registry.
5. deploy: Envia para a produção.

# Rodando o projeto PHP na máquina

Para rodar o projeto PHP com o objetivo de testar a imagem docker, faça os seguintes passos:

1. Crie a imagem docker com o build do projeto:

```console
docker build . -t php-docker
```

2. Execute o container:

```console
docker run -ti -p 80:80 php-docker
```

3. Análise de código estática:

```console
docker container run --rm -t -v "${PWD}":/workdir overtrue/phplint:latest ./ --exclude=vendor --no-configuration --no-cache
```

# Release e Deploy

Para fazer a release e deploy para produção está sendo utilizado um git flow
com a geração de tags para o projeto, seguindo o padrão [semantic versioning](https://semver.org).

Ou seja, quando uma [tag](https://gitlab.com/rafaelbmateus/gitlab-ci-example/-/tags) for criada
o CI irá fazer o build da imagem docker, publicar para o
[registry](https://gitlab.com/rafaelbmateus/gitlab-ci-example/container_registry) com a versão da tag
criada e sobscrevendo a tag latest.
